/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.BEAN;


public class BeanPublicaciones {
    private String not_id;
    private String not_titulo;
    private String not_texto;
    private String not_usu_id;


    public BeanPublicaciones(String not_id, String not_titulo, String not_texto, String not_usu_id) {
        this.not_id = not_id;
        this.not_titulo = not_titulo;
        this.not_texto = not_texto;
        this.not_usu_id = not_usu_id;
    }

    public BeanPublicaciones() {
    }

    public String getNot_id() {
        return not_id;
    }

    public void setNot_id(String not_id) {
        this.not_id = not_id;
    }

    public String getNot_titulo() {
        return not_titulo;
    }

    public void setNot_titulo(String not_titulo) {
        this.not_titulo = not_titulo;
    }

    public String getNot_texto() {
        return not_texto;
    }

    public void setNot_texto(String not_texto) {
        this.not_texto = not_texto;
    }

    public String getNot_usu_id() {
        return not_usu_id;
    }

    public void setNot_usu_id(String not_usu_id) {
        this.not_usu_id = not_usu_id;
    }
    
    
    
}


