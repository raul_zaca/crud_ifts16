package modelo.DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.BEAN.BeanPublicaciones;
import util.*;


public class DaoPublicaciones extends ClassConex implements  interfaceCRUD{
    
    public Connection conn=null;
    public Statement st=null;
    public ResultSet rs=null;
    
    
    public boolean encontrado=false;
    public boolean listo = false;
    
    public String not_id="";
    public String not_titulo="";
    public String not_texto="";
    public String not_usu_id="";
    
    
    
    public DaoPublicaciones(BeanPublicaciones cliente) {
        super();
        try {
            conn = this.ObtenerConexion();
            st = conn.createStatement();
            
            not_id = cliente.getNot_id();
            not_titulo =cliente.getNot_titulo();
            not_texto =cliente.getNot_texto();
            not_usu_id=cliente.getNot_usu_id();
            
        } catch (SQLException ex) {
            Logger.getLogger(DaoPublicaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
        
   @Override
    public boolean insertarRegistro() { //opcion 1.
        try {
            st.executeUpdate("insert into notas values('"+not_id+"','"+not_titulo+"','"+not_texto+"','"+not_usu_id+"');");
            listo=true;
           
        } catch (SQLException ex) {
            Logger.getLogger(DaoPublicaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listo;
    }

   
    @Override
    public boolean borrarRegistro() {// opcion 2.
        try {
            st.executeUpdate("delete from notas where not_id='"+not_id+"';");
            listo=true;
           
        } catch (SQLException ex) {
            Logger.getLogger(DaoPublicaciones.class.getName()).log(Level.SEVERE, null, ex);
      
        }
        return listo;
    }

    @Override
    public boolean actualizarRegistro() { //opcion 3.
        try {
            st.executeUpdate("update  notas set not_id='"+not_id+"',not_titulo='"+not_titulo+"',not_texto='"+not_texto+"' where not_id='"+not_id+"';");
            listo=true;
           
        } catch (SQLException ex) {
            Logger.getLogger(DaoPublicaciones.class.getName()).log(Level.SEVERE, null, ex);
      
        }
        return listo;
    }

    @Override
    public ResultSet consultarRegistro() { //opcion 4.
         try {        
            rs = st.executeQuery("SELECT *  FROM notas where not_id='"+not_id+"';");
        } catch (SQLException ex) {
            Logger.getLogger(DaoPublicaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
         return rs;
    }

    @Override
    public ResultSet listarTabla() { //opcion 5.
        try {        
            rs = st.executeQuery("SELECT not_id,not_titulo,not_texto,not_usu_id  FROM notas order by not_id;");
        } catch (SQLException ex) {
            Logger.getLogger(DaoPublicaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
         return rs;
        
    }
    
}
