/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.BEAN.BeanPublicaciones;
import modelo.DAO.DaoPublicaciones;


@WebServlet(name = "Servlet_publicaciones", urlPatterns = {"/Servlet_publicaciones"})
public class Servlet_publicaciones extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        int opcion=Integer.parseInt(request.getParameter("opcion"));
        String id=request.getParameter("not_id");
        String titulo=request.getParameter("not_titulo");
        String texto=request.getParameter("not_texto");
        String usuario=request.getParameter("not_usu_id");
        
    //___________________________________________________________________________________    
        BeanPublicaciones Bnota=new BeanPublicaciones(id,titulo,texto,usuario);
        DaoPublicaciones Dnota=new DaoPublicaciones(Bnota);
        ResultSet rs;
        
                   
         String mExito="Operacion exitosa...!"; 
         String mError="Operacion Fallida...!";
         
         switch(opcion){
            case 1:// AGREGAR REGISTROS
                if(Dnota.insertarRegistro()){
                    request.setAttribute("mensaje", mExito);
                }else{request.setAttribute("mensaje", mError);}
                
                request.getRequestDispatcher("index.jsp").forward(request, response);
                break;
            case 2://BORRAR REGISTROS

                
                if(Dnota.borrarRegistro()){
                    request.setAttribute("mensaje", mExito);
                }else{request.setAttribute("mensaje", mError);}
                
                request.getRequestDispatcher("index.jsp").forward(request, response);
            break;    
            case 3://ACTUALIZAR REGISTROS
                if(Dnota.actualizarRegistro()){
                    request.setAttribute("mensaje", mExito);
                }else{request.setAttribute("mensaje", mError);}
                
                request.getRequestDispatcher("index.jsp").forward(request, response);
                break;
            case 4://CONSULTAR UN REGISTRO
                rs=(ResultSet)Dnota.consultarRegistro();
        try {
            while(rs.next()){
                request.setAttribute("not_id", rs.getString(1));
                request.setAttribute("not_titulo", rs.getString(2));
                request.setAttribute("not_texto", rs.getString(3));
                request.setAttribute("not_usu_id", rs.getString(4));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Servlet_publicaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
                //request.setAttribute("resultset", rs);
                //request.setAttribute("resultset", rs);
                //request.setAttribute("resultset", rs);
                //request.setAttribute("resultset", rs);
                
                request.getRequestDispatcher("index.jsp").forward(request, response);
                break;
            case 5://LISTAR TODOS LOS REGISTROS
                rs=Dnota.listarTabla();
                request.setAttribute("resultset", rs);
                
                request.getRequestDispatcher("index.jsp").forward(request, response);
                break;
            default:
                
                request.getRequestDispatcher("index.jsp").forward(request, response);
                break;
         }
        
//        try {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Pagina respuesta</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Pagina respuesta</h1><br> opcion " + opcion);
//            out.println("<br>Identificacion:"+BCliente.getIdCliente());
//            out.println("<br>nombre:"+BCliente.getNombreCliente());
//            out.println("<br>Apellido:"+BCliente.getApellidoCliente());
//            out.println("<br>Genero:"+BCliente.getGeneroCliente());
//            
//            out.println("</body>");
//            out.println("</html>");
//        } finally {            
//            out.close();
//        }
//    
    }
    
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
